# Created by Patrick Mendus, Jean-Rodney Larrieux and Mustafa Kabukcu

This script requires the use of OpenCV, Python 3.6, urllib.request

$pip install opencv-python

Files included in this folder is :
	-Powerpoint Presenation of the project
	-Main.py 
		-Main script to run the aerial capture picture.
		-Output will be listed as output.jpeg within the folder.
		-Takes 5 arguments in the form minLat minLon maxLat maxLon level
    	-Level is optional. If no level is entered (between 1 and 23) it will default to 14
    	- EX: python main.py 41.9086744 -87.6818312 41.8097243 -87.6023617 14
	